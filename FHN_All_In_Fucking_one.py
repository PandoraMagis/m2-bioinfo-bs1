import os
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt


class FHN_Model:
    def __init__(
        self,
        modelTitle: str,
        time: int,
        timeStep: float,
        spaceWidth: int,
        spaceStep: float,
        b: float,
        a: float = 0.1,
        D: float = 1,
        paceMaker: bool = False,
        standarInitialCondition: bool = True,
    ) -> None:
        # Model naming (used for plot title)
        self.modelTitle = modelTitle

        #   Time and space parameters
        self.dt: float = timeStep
        self.dx: float = spaceStep
        self.TIME_STEPS, self.SPACE_STEPS = int(time / self.dt), int(
            spaceWidth / self.dx
        )

        #   Equation parameters
        self.eps = 0.01
        self.a = a  # 0.1
        self.b = b  # 2
        self.diffusion_value = D  # 1

        #   Matrix intialisation
        SPACE_STEPS, TIME_STEPS = self.SPACE_STEPS, self.TIME_STEPS
        # initiate U with -0.6 everywhere
        self.matrix_U_no_diffusion = np.full((SPACE_STEPS, TIME_STEPS), -0.6)
        self.matrix_U_diffusion = np.full((SPACE_STEPS, TIME_STEPS), -0.6)
        self.matrix_U_only_diffusion = np.full((SPACE_STEPS, TIME_STEPS), -0.6)
        # initiate V with -0.3 everywhere
        self.matrix_V_no_diffusion = np.full((SPACE_STEPS, TIME_STEPS), -0.3)
        self.matrix_V_diffusion = np.full((SPACE_STEPS, TIME_STEPS), -0.3)
        self.matrix_V_only_diffusion = np.full((SPACE_STEPS, TIME_STEPS), -0.3)

        print(
            f"number of TIME steps : {TIME_STEPS}, number of space slots : {SPACE_STEPS}"
        )
        print(
            f"matrix shape : {len(self.matrix_U_diffusion)} spaces slot and {len(self.matrix_U_diffusion[0])} TIME steps"
        )

        #   Equation initialisation
        self.u_red: np.ndarray = np.zeros(TIME_STEPS)
        self.u_blue: np.ndarray = np.zeros(TIME_STEPS)
        self.v_red: np.ndarray = np.zeros(TIME_STEPS)
        self.v_blue: np.ndarray = np.zeros(TIME_STEPS)

        self.u_blue[0] = -0.35  ### Membrane Potential (Mp) - red line
        self.u_red[0] = -0.25  ### Mb - blue line
        self.v_red[0], self.v_blue[0] = -0.3, -0.3  ### Blocking mechanism (Bm)

        #   Adding initial condition
        self.exitationPoints = []
        if standarInitialCondition:
            self.addPointInDict(
                (spaceWidth / self.dx) / 2, (spaceWidth / self.dx) / 20, 1
            )

        self.placeAllExitation()

        #   Adding other diffusion space values
        self.otherDiffusionSpace = []
        if paceMaker or self.b <= 0.8:
            self.adddiffusionSpace(
                (spaceWidth / self.dx) / 2, (spaceWidth / self.dx) / 20, 0.5
            )

        #   Adding new neurone activation at specific time step
        self.timeDelayActivation = []

    def addPointInDict(
        self, space: float, width: float, value: float, list: list = None
    ):
        list = self.exitationPoints if list is None else list
        list.append({"space": space, "width": width, "value": value})

    def adddiffusionSpace(
        self, space: float, width: float, value: float, list: list = None
    ):
        list = self.otherDiffusionSpace if list is None else list
        list.append({"space": space, "width": width, "value": value})

    def addTimeDelayedActivation(
        self, time: int, space: float, width: float, value: float, list: list = None
    ):
        list = self.timeDelayActivation if list is None else list
        list.append({"time": time, "space": space, "width": width, "value": value})

    # TODO both same code change that to sub order function

    def placeExitation(self, matrix, spacePos, exitationWidness, exitationValue):
        start = int(spacePos - exitationWidness)
        end = int(spacePos + exitationWidness)
        matrix[start:end] = exitationValue

    def placeAllExitation(self):
        ALL_MATRIX = [
            self.matrix_U_diffusion,
            self.matrix_U_no_diffusion,
            self.matrix_U_only_diffusion,
        ]

        for i in self.exitationPoints:
            print(
                f"Neurones at : {int(i['space'] - i['width'])},{int(i['space'] + i['width'])} exitated at {i['value']}"
            )

            for matrix in ALL_MATRIX:
                self.placeExitation(matrix, i["space"], i["width"], i["value"])

    def getNearby(self, matrix, space, j):
        if space == 0:
            return matrix[space + 1][j] - matrix[space][j]
        elif space == len(matrix) - 2:
            return matrix[space - 1][j] - matrix[space][j]
        else:
            return matrix[space + 1][j] + matrix[space - 1][j] - 2 * matrix[space][j]

    def getDiffusion(self, space):
        for i in self.otherDiffusionSpace:
            if i["space"] - i["width"] <= space <= i["space"] + i["width"]:
                return i["value"]
        return self.diffusion_value

    def getTimeDelayActivation(self, time, matrix):
        for point in self.timeDelayActivation:
            if point["time"] == time:
                start = point["space"] - point["width"]
                end = point["space"] + point["width"]
                for space in range(start, end):
                    matrix[space][time] = point["value"]

    def computeMatrix(self):
        ALL_MATRIX = [
            (self.matrix_U_only_diffusion, self.matrix_V_only_diffusion),
            (self.matrix_U_no_diffusion, self.matrix_V_no_diffusion),
            (self.matrix_U_diffusion, self.matrix_V_diffusion),
        ]
        ALL_EQUATION = [
            (self.u_red, self.v_red),
            (self.u_blue, self.v_blue),
        ]

        for time in range(0, self.TIME_STEPS - 1):

            #   Compute all the equation
            for U, V in ALL_EQUATION:
                PREVIOUS_U = U[time]
                PREVIOUS_V = V[time]

                EQUATION_U = PREVIOUS_U - PREVIOUS_U**3 - PREVIOUS_V
                EQUATION_V = self.eps * (PREVIOUS_U - self.b * PREVIOUS_V + self.a)

                U[time + 1] = PREVIOUS_U + self.dt * EQUATION_U
                V[time + 1] = PREVIOUS_V + self.dt * EQUATION_V

            #   Compute all the matrix
            for space in range(0, self.SPACE_STEPS - 1):
                #   commun to all matrix
                LOCAL_DIFUSION = self.getDiffusion(space) * self.dt / self.dx**2

                for count, (matrix_U, matrix_V) in enumerate(ALL_MATRIX):

                    PREVIOUS_U = matrix_U[space][time]
                    PREVIOUS_V = matrix_V[space][time]

                    # # on calcule les equations a l'instant T
                    EQUATION_U = self.dt * (PREVIOUS_U - PREVIOUS_U**3 - PREVIOUS_V)
                    EQUATION_V = self.dt * (
                        self.eps * (PREVIOUS_U - self.b * PREVIOUS_V + self.a)
                    )
                    # # U[i + 1] = U[i] + tau * (U[i] - U[i] ** 3 - V[i])
                    # # V[i + 1] = V[i] + tau * (eps * (U[i] - b * V[i] + a))

                    # On prends la valeur des voisin
                    valeurAvoisinanteU = self.getNearby(matrix_U, space, time)
                    valeurAvoisinanteV = self.getNearby(matrix_V, space, time)

                    DIFUSSION_U = LOCAL_DIFUSION * valeurAvoisinanteU
                    DIFUSSION_V = LOCAL_DIFUSION * valeurAvoisinanteV

                    if count == 0:
                        # ECRASE l'ancienne valeur de U et V
                        CURRENT_U = PREVIOUS_U + DIFUSSION_U
                        CURRENT_V = PREVIOUS_V + DIFUSSION_V
                    elif count == 1:
                        CURRENT_U = PREVIOUS_U + EQUATION_U
                        CURRENT_V = PREVIOUS_V + EQUATION_V
                    elif count == 2:
                        CURRENT_U = PREVIOUS_U + EQUATION_U + DIFUSSION_U
                        CURRENT_V = PREVIOUS_V + EQUATION_V + DIFUSSION_V
                    else:
                        raise ValueError(
                            "count is not in range, something went REALY wrong"
                        )

                    matrix_U[space][time + 1] = CURRENT_U  # if time != 2000 else 1
                    matrix_V[space][time + 1] = CURRENT_V  # if time != 2000 else 1

                    self.getTimeDelayActivation(time + 1, matrix_U)

    def visualisationMatrix(
        self, matrix, subName="", show=False, save=True, subPath=""
    ):
        # dynamic image title
        title = f"Heatmap of {self.modelTitle} - {subName}"

        fig, ax = plt.subplots(ncols=1, nrows=1)
        fig.tight_layout()

        im = ax.imshow(matrix, cmap="plasma", interpolation="nearest", aspect="auto")
        cbar = ax.figure.colorbar(im, ax=ax, shrink=0.5)
        ax.set_title(title)

        if save:
            plt.savefig(Path(f"figs{subPath}/{title}.png"), format="png", dpi=150)
        if show:
            plt.show()
        plt.clf()

    def visualisationEquation(self, subName="", show=False, save=True, subPath=""):
        # dynamic image title
        title = f"Equation of {self.modelTitle} - {subName}"

        # re-create time vector
        TIME = np.linspace(0, self.TIME_STEPS * self.dt, self.TIME_STEPS)

        plt.title(title)

        plt.plot(TIME, self.u_blue, color="blue")
        plt.plot(TIME, self.u_red, color="red")

        plt.axhline(y=-0.3, linestyle="--", color="black")
        plt.gca().set_ylim(-1.5, 1.5)
        plt.gca().set_xlim(0, 400)
        plt.xlabel("Time")
        plt.ylabel("U & U2")

        if save:
            plt.savefig(Path(f"figs{subPath}/{title}.png"), format="png", dpi=150)
        if show:
            plt.show()
        plt.clf()

    def visualisationPhasePortrait(self, subName="", show=False, save=True, subPath=""):
        def U_nullcline(f):
            return f - f**3

        def V_nullcline(f):
            return self.b * f - self.a

        title = f"Phase portait of {self.modelTitle} - {subName}"
        plt.title(title)
        plt.xlabel("V")
        plt.ylabel("U")

        U_nc = np.linspace(-1.5, 1.5, self.TIME_STEPS)
        V_nc = np.linspace(-1.5, 1.5, self.TIME_STEPS)

        plt.plot(U_nc, V_nullcline(V_nc), color="black", label="V nullcline")
        plt.plot(U_nullcline(U_nc), V_nc, color="grey", label="U nullcline")

        plt.plot(self.v_red, self.u_red, color="red", label="U = -0.35")
        plt.plot(self.v_blue, self.u_blue, color="blue", label="U2 = -0.25")

        plt.gca().set_ylim(-1.5, 1.5)
        plt.gca().set_xlim(-1, 1)

        if save:
            plt.savefig(Path(f"figs{subPath}/{title}.png"), format="png", dpi=150)
        if show:
            plt.show()
        plt.clf()

    def visualiseSlice(self, matrix, subtitle, time, show=False, save=True, subPath=""):
        # dynamic image title
        title = f"Slice {subtitle} at time {time}"

        # re-create time vector
        spaceLen = np.linspace(0, self.SPACE_STEPS * self.dx, self.SPACE_STEPS)

        plt.title(title)

        # take all value of the slice at a given time
        mat = [matrix[i][time] for i in range(len(matrix))]

        plt.plot(spaceLen, mat, color="pink")

        # plt.xlabel("Time")
        # plt.ylabel("U & U2")

        if save:
            plt.savefig(Path(f"figs{subPath}/{title}.png"), format="png", dpi=150)
        if show:
            plt.show()
        plt.clf()

    def visualisationAll(self):
        os.makedirs(Path(f"figs/{self.modelTitle}"), exist_ok=True)
        ALL_U_MATRIX = [
            ("only diffusion - no equation", self.matrix_U_only_diffusion),
            ("without diffusion", self.matrix_U_no_diffusion),
            ("with diffusion", self.matrix_U_diffusion),
        ]
        for subtitle, matrix in ALL_U_MATRIX:
            self.visualisationMatrix(
                matrix, subName=subtitle, subPath=f"/{self.modelTitle}"
            )
        self.visualisationEquation(subPath=f"/{self.modelTitle}")
        self.visualisationPhasePortrait(subPath=f"/{self.modelTitle}")
