import random
from FHN_All_In_Fucking_one import FHN_Model

# myModel = FHN_Model("testSlice", 400, 0.1, 400, 1, 1)
# myModel.visualiseSlice(myModel.matrix_U_only_diffusion, "startPosition", 0)
# myModel.computeMatrix()
# myModel.visualiseSlice(myModel.matrix_U_only_diffusion, "endPosition", 4000 - 1)

# ### Make all standar figure
# figs: dict = {"Bistable": 2, "Excitable": 1.5, "Oscillatory": 1.0}
# for figName in figs:
#     myModel = FHN_Model(
#         figName, 400, 0.1, 400, 1, figs[figName], paceMaker=(figName == "Oscillatory")
#     )
#     myModel.computeMatrix()
#     myModel.visualisationAll()
#     # myModel.visualisationMatrix(
#     #     myModel.matrix_U_diffusion, "with diffusion", show=True, save=False
#     # )


# #   random time activations
time = 400
dt = 0.1
space = 400
dx = 1
b = 1.0

# randomTime = FHN_Model("random_time_activation", time, dt, space, dx, b)
randomDiffusion = FHN_Model("diffusion_Low_And_Hight", time, dt, space, dx, b)
DiffusionHight = FHN_Model("diffusion_Low", time, dt, space, dx, b)
DiffusionLow = FHN_Model("diffusion_Hight", time, dt, space, dx, b)

DiffusionHight.adddiffusionSpace(200, 200, 4)
DiffusionLow.adddiffusionSpace(200, 200, 0.2)
randomDiffusion.adddiffusionSpace(100, 50, 4)
randomDiffusion.adddiffusionSpace(300, 50, 0.2)

randomDiffusion.computeMatrix()
DiffusionLow.computeMatrix()
DiffusionHight.computeMatrix()

randomDiffusion.visualisationMatrix(
    randomDiffusion.matrix_U_diffusion, "diffusion hight top, low bottom"
)
DiffusionLow.visualisationMatrix(
    DiffusionLow.matrix_U_diffusion, "diffusion hight all along"
)
DiffusionHight.visualisationMatrix(
    DiffusionHight.matrix_U_diffusion, "diffusion low all along"
)


# randomInitial = FHN_Model(
#     "random_initial_condition", time, dt, space, dx, b, standarInitialCondition=False
# )
# randomFull = FHN_Model(
#     "randomise_ALL", time, dt, space, dx, b, standarInitialCondition=False
# )

# for i in range(0, 3):
#     atTime = random.randint(10, (time * dt) / 2)
#     atSpace = random.randint(10, space)
#     withWidth = random.randint(3, 10)
#     withVal = 1 if random.random() > 0.5 else -0.75
#     randomTime.addTimeDelayedActivation(atTime, atSpace, withWidth, withVal)
#     randomFull.addTimeDelayedActivation(atTime, atSpace, withWidth, withVal)


# for i in range(0, 3):
# atSpace = random.randint(10, space)
# withWidth = random.randint(3, 10)
# withVal = random.random() * 2  # range = float[0:2]
# #randomDiffusion.adddiffusionSpace(100, 50, 4)
# #randomDiffusion.adddiffusionSpace(300, 50, 0.2)
# randomFull.adddiffusionSpace(atSpace, withWidth, withVal)


# for i in range(0, 3):
#     atSpace = random.randint(10, space)
#     withWidth = random.randint(3, 10)
#     withVal = 1 if random.random() > 0.5 else -0.75
#     randomInitial.addPointInDict(atSpace, withWidth, withVal)
#     randomFull.addPointInDict(atSpace, withWidth, withVal)

# print("random Diffusion ")
# print("random Initial ")
# randomInitial.computeMatrix()
# print("random Time ")
# randomTime.computeMatrix()
# print("random Full ")
# randomFull.computeMatrix()

# randomInitial.visualisationAll()
# randomTime.visualisationAll()
# randomFull.visualisationAll()

# randomFull.visualisationMatrix(
#     randomFull.matrix_U_diffusion, "all random", show=True, save=False
# )
