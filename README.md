# M2-BioInfo-BS1 - Modéle FHN

Voici notre git, support d'echange de codes et de gestion de version.

## Utiliser le code

Pour generer toutes les graphs des figure 2 et 3 de l'article un fichier [allDigs.bat](https://gitlab.com/PandoraMagis/m2-bioinfo-bs1/-/blob/main/allFigs.bat) destiné a une utilisation Windows est disponible.

Plusieurs fichiers sont present :

Le [Modéle de base](https://gitlab.com/PandoraMagis/m2-bioinfo-bs1/-/blob/main/FHN_Model.py) vous permetras d'obtenir les graphs correspondant a la figure 2 de l'[Article](<https://gitlab.com/PandoraMagis/m2-bioinfo-bs1/-/blob/main/Gelens-MBoC14%20(1).pdf>)
Utilisation : `python .\FHN_Model.py x` Ou x = {A,B,C,D,E,F} pour le graphs correspondant au graph du meme nom dans la figure 2

Le [Modéle avec diffusion](https://gitlab.com/PandoraMagis/m2-bioinfo-bs1/-/blob/main/FHN_Diffusion.py) permet de generer les images de diffusion constituant la figure 3.

L'[Approche programtion objet](https://gitlab.com/PandoraMagis/m2-bioinfo-bs1/-/blob/main/FHN_Diffusion.py) permet elle de generer un modéle FHN selon ce propres paramétres, permet de faire du bruit des differents types.

## TODO

pydoc on [that file](https://gitlab.com/PandoraMagis/m2-bioinfo-bs1/-/blob/main/FHN_Diffusion.py)
