import numpy as np
import matplotlib.pyplot as plt
import sys


#################################################################################
###                  BISTABILITY - EXCITABILITY - OSCILLATORY                 ###
#################################################################################

######################################### Parameters common to all
nt = 4000  ### Initialisation du nombre de pas de temps total
tau = 0.1  ### Initialisation du pas de temps

if len(sys.argv) == 3:
    nt = int(sys.argv[2])

T = [i * tau for i in range(nt)]
# np.zeros(()) Creation of a table of n rows (4000) over 1 column of 0
######################################### Default arguments "A" ou "B":
U = np.zeros((nt, 1))
U2 = np.zeros((nt, 1))
V = np.zeros((nt, 1))
V2 = np.zeros((nt, 1))
aff = np.zeros((nt, 1))
aff[0] = -1

U[0] = -0.25  ### Membrane Potential (Mp) - red line
U2[0] = -0.35  ### Mb - blue line
V[0] = -0.3  ### Blocking mechanism (Bm)
V2[0] = -0.3
eps = 0.01  ### Coefficient added to the Bm in order to make it slow
a = 0.1  ### Leak of K+ from the extraC to the intraC
b = 2  ### Strength of the Bm.
# if b is low, the Bm will be really fast to block the Voltage
# if b is high, the Voltage will take more time to come back to the resting potential
# b > 1.8 to be in a bistable state

if sys.argv[1] in "CD":
    b = 1.5
elif sys.argv[1] in "EF":
    b = 1.0

###################################################### Algorithm
################################################################
for i in range(0, nt - 1):
    U[i + 1] = U[i] + tau * (U[i] - U[i] ** 3 - V[i])
    U2[i + 1] = U2[i] + tau * (U2[i] - U2[i] ** 3 - V2[i])
    V[i + 1] = V[i] + tau * (eps * (U[i] - b * V[i] + a))
    V2[i + 1] = V2[i] + tau * (eps * (U2[i] - b * V2[i] + a))
    # U_nc[i+1] = U_nc[i] + tau * eps
    # V_nc[i+1] = V_nc[i] + tau *(U[i] - U[i]**3)
    T[i + 1] = T[i] + tau

##################################################### Nullclines
################################################################
def U_nullcline(f):
    return f - f**3


def V_nullcline(f):
    return b * f - a


U_nc = np.linspace(-1.5, 1.5, 4000)
V_nc = np.linspace(-1.5, 1.5, 4000)

################################################## Visualization
################################################################
def plot_Figure(figure_nbr):

    # configure plot tools for the figure A, C or E (same configurations)
    if figure_nbr in "ACE":
        plt.plot(T, U, color="red")
        plt.plot(T, U2, color="blue")
        # plt.plot(T,V, color="grey", linestyle="dotted")
        plt.axhline(y=-0.3, linestyle="--", color="black")
        plt.gca().set_ylim(-1.5, 1.5)
        plt.gca().set_xlim(0, 400)
        plt.title(f"Dynamic for b = {b}")
        plt.xlabel("Time")
        plt.ylabel("U & U2")

    elif sys.argv[1] in "BDF":
        plt.plot(U_nc, V_nullcline(V_nc), color="black", label="V nullcline")
        plt.plot(U_nullcline(U_nc), V_nc, color="grey", label="U nullcline")

        plt.plot(V, U, color="red", label="U = -0.35")
        plt.plot(V2, U2, color="blue", label="U2 = -0.25")

        plt.title("U,V two variable system, phase portrait")
        plt.xlabel("V")
        plt.ylabel("U")
        plt.gca().set_ylim(-1.5, 1.5)
        plt.gca().set_xlim(-1, 1)
        plt.legend(loc="upper right")

    plt.show()
    plt.savefig(f"figs/Figure_{figure_nbr}.png")


plot_Figure(sys.argv[1])


#################################################################################
###                             ADDING DIFFUSION                              ###
#################################################################################

for i in range(0, nt-1):
    U[i+1] = U[i] + tau * (U[i] - U[i]**3 - V[i])
    U2[i+1] = U2[i] + tau * (U2[i] - U2[i]**3 - V2[i])
    V[i+1] = V[i] + tau * (eps * (U[i] - b*V[i] + a))
    V2[i+1] = V2[i] + tau * (eps * (U2[i] - b*V2[i] + a))
    #U_nc[i+1] = U_nc[i] + tau * eps
    #V_nc[i+1] = V_nc[i] + tau *(U[i] - U[i]**3)
    T[i+1] = T[i] + tau