from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
import sys

np.set_printoptions(
    threshold=sys.maxsize
)  # pour afficher tout le numpy array quand on fait un priTIME
######################################### Parameters common to all
TIME = 400  ### Initialisation du nombre de pas de temps total
dt = 0.1  ### Initialisation du pas de temps
dx = 1
SPACE = 400  # height of the matrix
TIME_STEPS, SPACE_STEPS = int(TIME / dt), int(SPACE / dx)

eps = 0.01
a = 0.1
b = 2

D = 1  # Diffusion
# TODO : PLUS CLEAN ICI
D = float(sys.argv[2]) if len(sys.argv) > 2 else D

######################################### matrices U et V
# creation of the matrix
exitationWidness = (SPACE / dx) / 20  # = 180:220
exitationPoints = [(SPACE / dx) / 2]

# initialiser la matrice U à -0.6 et remplacer de 180 à 220 par 1
matrix_U = np.full((SPACE_STEPS, TIME_STEPS), -0.6)
# initialiser la matrice V à -0.3 prtt
matrix_V = np.full((SPACE_STEPS, TIME_STEPS), -0.3)

# validation print
print(f"number of TIME steps : {TIME_STEPS}, number of space slots : {SPACE_STEPS}")
print(f"matrix shape : {len(matrix_U)} spaces slot and {len(matrix_U[0])} TIME steps")

# creation of points of exitation
for spacePos in exitationPoints:
    start = int(spacePos - exitationWidness)
    end = int(spacePos + exitationWidness)
    print(f"Exitated neurones on : {start},{end}")
    matrix_U[start:end] = 1

if sys.argv[1] == "C":
    b = 1.5
elif sys.argv[1] == "B":
    b = 2
elif sys.argv[1] == "D":
    b = 1.0  # 1.55 look like better

###################################################### Algorithm
################################################################


def getNearby(matrix, space, j):
    # return matrix[space + 1][j] + matrix[space - 1][j] - 2 * matrix[space][j]
    if space == 0:
        return matrix[space + 1][j] - matrix[space][j]
    elif space == len(matrix) - 2:
        return matrix[space - 1][j] - matrix[space][j]
    else:
        return matrix[space + 1][j] + matrix[space - 1][j] - 2 * matrix[space][j]


for j in range(0, TIME_STEPS - 1):
    for i in range(0, SPACE_STEPS - 1):
        # j 0 -> 3999 AKA LE TEMPS
        # i 0 -> 3999 AKA l'espcaes
        # i 0 -> 3999
        # on fait {1 --> 3999} j fois CAD on check tt l'espace a chq temps

        local_diff = D
        # pace maker occilatory
        if (
            sys.argv[1] != "A"
            and exitationPoints[0] - exitationWidness
            <= i
            <= exitationPoints[0] + exitationWidness
        ):
            local_diff = 0.5

        PREVIOUS_U = matrix_U[i][j]
        PREVIOUS_V = matrix_V[i][j]

        # # on calcule les equations a l'instant T
        EQUATION_U = dt * (PREVIOUS_U - PREVIOUS_U**3 - PREVIOUS_V)
        EQUATION_V = dt * (eps * (PREVIOUS_U - b * PREVIOUS_V + a))

        # # assert dt / (dx**2) == 0.1
        coefDiffusion = local_diff * dt / dx**2

        # On prends la valeur des voisin
        valeurAvoisinanteU = getNearby(matrix_U, i, j)
        valeurAvoisinanteV = getNearby(matrix_V, i, j)

        DIFUSSION_U = coefDiffusion * valeurAvoisinanteU
        DIFUSSION_V = coefDiffusion * valeurAvoisinanteV
        # # U[i + 1] = U[i] + tau * (U[i] - U[i] ** 3 - V[i])
        # # V[i + 1] = V[i] + tau * (eps * (U[i] - b * V[i] + a))

        if sys.argv[1] == "A":
            # ECRASE l'ancienne valeur de U et V
            CURRENT_U = PREVIOUS_U + DIFUSSION_U
            CURRENT_V = PREVIOUS_V + DIFUSSION_V
        elif D == 0:
            CURRENT_U = PREVIOUS_U + EQUATION_U
            CURRENT_V = PREVIOUS_V + EQUATION_V
        else:
            CURRENT_U = PREVIOUS_U + EQUATION_U + DIFUSSION_U
            CURRENT_V = PREVIOUS_V + EQUATION_V + DIFUSSION_V

        matrix_U[i][j + 1] = CURRENT_U  # if j < 2000 else 1
        matrix_V[i][j + 1] = CURRENT_V


################################################# Vizualization
###############################################################


def visualisation(matrix, name="test", subName="", show=False):
    # dynamic image title
    title = f"Heatmap of Figure 3{name} - {'no diffusion' if D ==0 else 'with diffsusion ='+str(D)} - {subName}"
    # title = f"Heatmap of Figure - D = ICICI - We Are Testing"

    fig, ax = plt.subplots(ncols=1, nrows=1)
    fig.tight_layout()

    im = ax.imshow(matrix, cmap="cool", interpolation="nearest", aspect="auto")
    cbar = ax.figure.colorbar(im, ax=ax, shrink=0.5)
    ax.set_title(title)

    plt.savefig(Path(f"figs/{title}.png"), format="png", dpi=150)
    if show:
        plt.show()
    plt.clf()


if sys.argv[1] == "A":
    # for j in range(0, TIME - 1):
    #     for i in range(0, SPACE - 1):
    #         matrix_U[i][j + 1] = matrix_U[i][j] + D1 * dt / dx**2 * (
    #             matrix_U[i + 1][j] + matrix_U[i - 1][j] - 2 * matrix_U[i][j]
    #         )
    visualisation(matrix_U, name="A", subName="no reaction")
elif sys.argv[1] in "B":
    visualisation(matrix_U, name="B", subName="bistable")
elif sys.argv[1] in "C":
    visualisation(matrix_U, name="C", subName="excitable")
elif sys.argv[1] == "D":
    visualisation(matrix_U, name="D", subName="oscillatory")


# visualisation(matrix_U, name="TEST", subName="ON DEV", show=False)
